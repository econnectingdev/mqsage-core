First table Speedup # processors vs average time for each abc_i_j_k with 0 < k < n
#processors             1            2           4           8              
avg. time  abc_i_j_k 
avg. time  abc_i_j_k   

Examples

 
##MQWriter([["abc",2,2,2]])
##MQWriter([["sts",16,4,8,2]])
##MQWriter([["sts",16,5,8,2]])
##MQWriter([["sts",32,4,8,2]])
##MQWriter([["hfe",2,4,2]])
##MQWriter([["hfe",10,16,2]])  
#
#
#
#'''
#MQWriter([["abc",4,2,1,31]])
#MQWriter([["abc",2,2,3,33]])
#MQWriter([["abc",2,2,4,40]])
#MQWriter([["abc",3,2,3,50]])
#MQWriter([["abc",8,2,1,55]])
#MQWriter([["abc",3,2,4,60]])
#
#MQWriter([["hfe",6,5,2,31]])
#MQWriter([["hfe",8,4,2,33]])
#MQWriter([["hfe",8,5,2,40]])
#MQWriter([["hfe",7,7,2,50]])
#MQWriter([["hfe",9,6,2,55]])
#MQWriter([["hfe",8,7,2,60]])
#
#MQWriter([["sts",7,5,3,2,31]])
#MQWriter([["sts",4,4,5,2,33]])
#MQWriter([["sts",4,4,7,2,40]])
#MQWriter([["sts",4,4,9,2,50]])
#MQWriter([["sts",9,9,4,2,55]])
#MQWriter([["sts",8,7,6,2,60]])


##for i in range(50):
#    #MQWriter([["abc",2,2,1,0,i]])
#    #MQWriter([["abc",3,2,1,0,i]])
#    #MQWriter([["abc",5,2,1,0,i]])
#    #MQWriter([["abc",5,2,1,0,i]])
#    #MQWriter([["abc",6,2,1,0,i]])
#    #MQWriter([["abc",7,2,1,0,i]])
#    #MQWriter([["abc",8,2,1,0,i]])
#    #MQWriter([["abc",10,2,1,0,i]])
#    #MQWriter([["abc",11,2,1,0,i]])
#    #MQWriter([["abc",12,2,1,0,i]])
#
#
##MQWriter([["hfe",80,70,2,1]])
##MQWriter([["sts",70,6,1,2,60,1]])
#"""
#for i in range(50):
#   MQWriter([["abc",3,2,1,0,i]])
#
#"""
#"""
#from mpi4py import MPI
#comm = MPI.COMM_WORLD
#nprocs = 4
#per_node = 15 
#for i in range(0,nprocs):
#    if comm.rank == i:
#        for j in range(per_node):
#            #MQWriter([["abc",3,2,1,0,(i)*per_node+j+1]])
#            MQWriter([["sts",70,6,1,2,60,1]])
##for i in range(50):
#                   #s,q,pot
#   #MQWriter([["abc",2,2,1,0,i]])    
#                    #L,r,pot,q 
#   #MQWriter([["sts",8,7,6,2,i]])
#                   #pot,r,q
#   #MQWriter([["hfe",2,10,2,i]])
##uov, q, p, n, m,iter
#"""
#MQWriter([["uov",2,2,4,2,0]])
