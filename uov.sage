"""
Rainbow Implementation according thesis Albretch

AUTHOR: Juan Grados <juancgv@lncc.br>, Pedro Lara, Renato Portugal
"""
import sys
from sage.all import *
from sage.sat.converters.polybori import CNFEncoder
from sage.sat.solvers.dimacs import DIMACS
from sage.crypto.mq.sbox import SBox
from sage.crypto.mq.mpolynomialsystemgenerator import MPolynomialSystemGenerator
from sage.sat.boolean_polynomials import solve as solve_sat
#from sage.crypto.mq.mpolynomialsystem import MPolynomialSystem, MPolynomialRoundSystem
from sage.rings.polynomial.multi_polynomial_sequence import PolynomialSequence
from sage.rings.polynomial import *
#from sage.misc.functional import parent
from sage.misc.cachefunc import cached_function
from sage.modules.free_module import VectorSpace, FreeModule

from sage.rings.all import FiniteField as GF
from sage.rings.integer_ring import ZZ
from sage.rings.polynomial.polynomial_ring_constructor import BooleanPolynomialRing_constructor as BooleanPolynomialRing
from sage.rings.polynomial.pbori import BooleanPolynomialRing as BooleanPolynomialRing_class
import hashlib
import random
import binascii
import string

class UOV:

    def __init__(self, q, pot, n, m):
        self.m = m
        self.n = n
        self.pot = pot
        self.K = GF(q**pot,"t")
    
    #toy example of the book Multivariate Public Key Cryptosystems Jintai Ding and et al
    def test_key_generation(self):
        t = self.K.gen()
        Q1 = matrix(self.K, self.n, self.n)
        #Q1[0,0] = 0; Q1[0,1] = 0; Q1[0,2] = 0; Q1[0,3] = 1;    Q1[0,4] = t**2; Q1[0,5] = t**2;
        #Q1[1,0] = 0; Q1[1,1] = 0; Q1[1,2] = 0; Q1[1,3] = 1;    Q1[1,4] = t;    Q1[1,5] = 1;
        #Q1[2,0] = 0; Q1[2,1] = 0; Q1[2,2] = 0; Q1[2,3] = t**2; Q1[2,4] = t**2; Q1[2,5] = t**2;
        Q1[0,0] = 0; Q1[0,1] = 0; Q1[0,2] = 0; Q1[0,3] = t;    Q1[0,4] = 0;    Q1[0,5] = 0;
        Q1[1,0] = 0; Q1[1,1] = 0; Q1[1,2] = 0; Q1[1,3] = 0;    Q1[1,4] = t**2; Q1[1,5] = 1;
        Q1[2,0] = 0; Q1[2,1] = 0; Q1[2,2] = 0; Q1[2,3] = 0;    Q1[2,4] = 0;    Q1[2,5] = 1;
        Q1[3,0] = 0; Q1[3,1] = 0; Q1[3,2] = 0; Q1[3,3] = 1;    Q1[3,4] = t**2; Q1[3,5] = t**2;
        Q1[4,0] = 0; Q1[4,1] = 0; Q1[4,2] = 0; Q1[4,3] = 1;    Q1[4,4] = t;    Q1[4,5] = 1;
        Q1[5,0] = 0; Q1[5,1] = 0; Q1[5,2] = 0; Q1[5,3] = t**2; Q1[5,4] = t**2; Q1[5,5] = t**2;
        Q2 = matrix(self.K, self.n, self.n)
        Q2[0,0] = 0; Q2[0,1] = 0; Q2[0,2] = 0; Q2[0,3] = 0;    Q2[0,4] = t;    Q2[0,5] = t;
        Q2[1,0] = 0; Q2[1,1] = 0; Q2[1,2] = 0; Q2[1,3] = 1;    Q2[1,4] = t**2; Q2[1,5] = t;
        Q2[2,0] = 0; Q2[2,1] = 0; Q2[2,2] = 0; Q2[2,3] = t;    Q2[2,4] = 1;    Q2[2,5] = t**2;
        Q2[3,0] = 0; Q2[3,1] = 0; Q2[3,2] = 0; Q2[3,3] = 1;    Q2[3,4] = t;    Q2[3,5] = 1;
        Q2[4,0] = 0; Q2[4,1] = 0; Q2[4,2] = 0; Q2[4,3] = 0;    Q2[4,4] = 0;    Q2[4,5] = 0;
        Q2[5,0] = 0; Q2[5,1] = 0; Q2[5,2] = 0; Q2[5,3] = 0;    Q2[5,4] = 0;    Q2[5,5] = 1;
        Q3 = matrix(self.K, self.n, self.n)
        Q3[0,0] = 0; Q3[0,1] = 0; Q3[0,2] = 0; Q3[0,3] = t;    Q3[0,4] = t;    Q3[0,5] = 0;
        Q3[1,0] = 0; Q3[1,1] = 0; Q3[1,2] = 0; Q3[1,3] = 1;    Q3[1,4] = 0;    Q3[1,5] = 1;
        Q3[2,0] = 0; Q3[2,1] = 0; Q3[2,2] = 0; Q3[2,3] = t**2; Q3[2,4] = 1;    Q3[2,5] = t**2;
        Q3[3,0] = 0; Q3[3,1] = 0; Q3[3,2] = 0; Q3[3,3] = 0;    Q3[3,4] = t**2; Q3[3,5] = 1;
        Q3[4,0] = 0; Q3[4,1] = 0; Q3[4,2] = 0; Q3[4,3] = 0;    Q3[4,4] = 0;    Q3[4,5] = t;
        Q3[5,0] = 0; Q3[5,1] = 0; Q3[5,2] = 0; Q3[5,3] = 0;    Q3[5,4] = 0;    Q3[5,5] = 0
        T = matrix(self.K, self.n, self.n)
        T[0,0] = 1;   T[0,1] = t**2; T[0,2] = t; T[0,3] = t;    T[0,4] = 0;    T[0,5] = t**2;
        T[1,0] = t**2;T[1,1] = t**2; T[1,2] = 1; T[1,3] = 1;    T[1,4] = 1;    T[1,5] = t;
        T[2,0] = 1;   T[2,1] = 0;    T[2,2] = 1; T[2,3] = t**2; T[2,4] = 1;    T[2,5] = t**2;
        T[3,0] = t;   T[3,1] = t;    T[3,2] = 1; T[3,3] = t;    T[3,4] = 0;    T[3,5] = 1;
        T[4,0] = t;   T[4,1] = 1;    T[4,2] = t; T[4,3] = t**2; T[4,4] = 0;    T[4,5] = t**2;
        T[5,0] = 1;   T[5,1] = 1;    T[5,2] = 1; T[5,3] = t;    T[5,4] = t;    T[5,5] = 0
        T_trans = transpose(T)
        lst = []
        lst_pub = []
        lst.append(Q1);lst.append(Q2);lst.append(Q3)
        for k in range(self.m):
            lst_pub.append(T_trans*lst[k]*T)
        sk = [T, lst]
        pretty_print(sk)
        pretty_print(pk)
        return sk, pk
        
    def key_generation(self):
        
        while(true):
            T = random_matrix(self.K, self.n, self.n,sparse=True,density=0.9)
            if (T.rank() == self.n):
                break
        T_trans = transpose(T)
        lst = []
        lst_pub = []
        for k in range(self.m):
            P = matrix(self.K, self.n, self.n)
            for i in range(self.n):
                for j in range(self.n):
                    if (j >= i and i <= self.n - self.m - 1):
                        P[i,j] = self.K.random_element()
            lst.append(P)
            lst_pub.append(T_trans*lst[k]*T)
        pk = lst_pub
          
        sk = [T, lst]
        pretty_print(sk)
        pretty_print(pk)
        return sk, pk
     

    def sign_generation(self, sk, d):
        T = sk[0]
        lst_P = sk[1]
        vin = random_matrix(self.K, 1, self.n-self.m)
        t = self.K.gen()
        #creating the linear system
	A = matrix(self.K, self.m, self.m)
        b = vector(self.K,self.m)
        for i in range(self.m):
            for j in range(self.m):
                sum = 0
                for k in range(self.m):
                    sum = sum + vin[0][k]*lst_P[i][k][self.n-self.m+j]
                A[i,j] = sum        
            sumb = 0
            for ii in range(self.n-self.m):
                for jj in range(self.n-self.m):
                    sumb = sumb + lst_P[i][ii][jj]*vin[0][ii]*vin[0][jj]
            b[i] = sumb + d[i]
        pretty_print(A)
        pretty_print(b)
        xx = A\b
        #print 'vin', vin
        #print 'solution', xx
        y = vector(self.K, self.n)
        for i in range(self.n):
            if (i<self.m):
                y[i] = vin[0][i]
            else:
                y[i] = xx[i-m]
        #check solution
        """
        print 'check_vector',y
        print y*lst_P[1]*y
        """
        T_inv = T.inverse()
        return T.inverse()*y
    
    def evalua(self, lst_pk, sign):
        evalu = vector(self.K,self.m)
        for k in range(self.m):
            evalu[k] = sign*lst_pk[k]*sign
        return evalu

    def verification(self, lst_pk, sign, d):
        for k in range(self.m):
            if (sign*lst_pk[k]*sign != d[k]):
                return 0
        return 1
    def get_finitefield(self):
        return self.K

    def rep(self,ele):
        if self.pot == 1:
            return str(ele)
        else:
            return str(ele.int_repr())
    def printPublicKey_ToolANF(self, pk, c, v, iter): #falta arrumar v
        # m oil variables
        # n-m vinegar variables
        
        f = open(path_output+"uov_"+str(self.m)+"_"+str(self.n-self.m)+"_"+str(self.pot)+"_"+str(iter)+".mq",'w')
        
        f.write(str(self.m) + " " + str(self.n) + " " + str(self.K.modulus().degree())+"\n")
        linha = ""
        for i in range(self.m):
            linha = linha + self.rep(c[i]) + " "
        #print linha
        f.write(linha+"\n")
        
        for k in range(self.m):
            for i in range(self.n):
                #print "i:", i
                linha = ""
                for j in range(i+1):
                    #print j
                    linha = linha + str(self.rep(pk[k][i][j])) + " "
                #print linha
                f.write(linha+"\n")
            f.write(v+"\n")
            #print "\n"
        
        f.close()
        
"""
n = 6
m = 3
uov = UOV(2, 2, n, m)
[sk,pk] = uov.key_generation()
#[sk,pk] = uov.test_key_generation()
gf = uov.get_finitefield()
t = gf.gen()
d = vector([gf.random_element() for iii in range(n)])
#d = vector([t,1,t**2])#for test Jintai Ding book
print 'document', d
sign = uov.sign_generation(sk, d)
print uov.verification(pk, sign, d)
#uov.test_key_generation()
"""
