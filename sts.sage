#*****************************************************************************
#       Copyright (C) 2010 Juan Grados Vasquez juaninf@lncc.br
#
#  Distributed under the terms of the GNU General Public License (GPL)
#  as published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#                  http://www.gnu.org/licenses/
#*****************************************************************************
r"""
regular STS scheme Implementation on field with characteristic odd and high rank attack

AUTHORS:

- Juan Grados, Pedro Lara, Renato Portugal (2014): initial version
"""
import sys
from sage.all import *
from sage.sat.converters.polybori import CNFEncoder
from sage.sat.solvers.dimacs import DIMACS
from sage.crypto.mq.sbox import SBox
from sage.crypto.mq.mpolynomialsystemgenerator import MPolynomialSystemGenerator
from sage.sat.boolean_polynomials import solve as solve_sat
#from sage.crypto.mq.mpolynomialsystem import MPolynomialSystem, MPolynomialRoundSystem
from sage.rings.polynomial.multi_polynomial_sequence import PolynomialSequence
from sage.rings.polynomial import *
#from sage.misc.functional import parent
from sage.misc.cachefunc import cached_function
from sage.modules.free_module import VectorSpace, FreeModule

from sage.rings.all import FiniteField as GF
from sage.rings.integer_ring import ZZ
from sage.rings.polynomial.polynomial_ring_constructor import BooleanPolynomialRing_constructor as BooleanPolynomialRing
from sage.rings.polynomial.pbori import BooleanPolynomialRing as BooleanPolynomialRing_class

from abc import *
import hashlib
import random
import binascii
import string

class STS:
    def __init__(self, L, r, pot, q, n):
        print "STS"
        self.modePrint = 1
        self.L = L
        self.r = r
        self.q = q
        self.pot = pot 
        self.m = L*r
        #self.n = L*r
        self.n = n
        K = GF(q**pot,"t")
        self.K = K
        self.car = 2 if q%2==0 else 1
    @staticmethod
    def binary2str(message):
        """Convert binary vector on K to string.
    
        INPUT:
        - 'message' - vector on K with size n
        """
        s = ""
        for i in range(len(message)):
            s = s + str(message[i])
        return s
    
    def getLowerMatrix(self,M):
        """Convert matrix representation of quadratic polynomial in lower triangular matrix.
    
        INPUT:
    
        - 'M' - matrix on K
        """
        for i in range(self.n):
            for j in range(self.n):
                if (i>j):
                    M[i,j] = M[i,j] + M[j,i]
                    M[j,i] = 0
        return M
    
    def randomMatrixSTS(self,K,l,n,r):
        """Get a random matrix representation for quadratic polynomial for regular STS scheme
    
        INPUT:
    
        - K - field
        - l - index of polynomial (p_1,...,p_l,...p_n)
        - n - L*r
        - r - parameter of STS scheme
        """
        l = l+1
        M = matrix(self.K,self.n,self.n)
        for i in range(n):#modify by juan grados 04/04/2016 self.r*l
            for j in range(n):#modify by juan grados 04/04/2016 self.r*l
                M[i,j] = self.K.random_element()
        if self.car == 1:
            return (self.getLowerMatrix(M)+self.getLowerMatrix(M).transpose())/(self.K(2))
        else:
            return M
            
    
    def keyGeneration(self):
        """Get public and secret keys for STS scheme
    
        INPUT:
        
        - 'K' - field
    
        OUTPUT:
    
        - 'pk_lst' - list of matrix's representing the public quadratic polynomials
        - 'S' - Linear Transformation 
        - 'T' - Linear Transformation 
        - 'sk_lst' - list of matrix's representing the private quadratic polynomials
        """
        #generating T e S
        pk_lst = []
        sk_lst = []
        for l in range(self.L):
            for i in range(self.r):
                sk_lst.append(self.randomMatrixSTS(self.K,l,self.n,self.r))
        while(true):
            S = random_matrix(self.K, self.n, self.n,sparse=True,density=0.9)
            T = random_matrix(self.K, self.m, self.m,sparse=True,density=0.9)
            if (S.rank()==self.n and T.rank()==self.m):
                break
        lst_temp = []
        lst = []
        S_trans = transpose(S)
        for j in range(self.m):
            lst_temp.append(S_trans*sk_lst[j]*S)
        for i in range(self.m):
            M = matrix(self.K,self.n,self.n)
            for j in range(self.m):
                M = M + T[i,j]*lst_temp[j]
            if self.car == 1:
               pk_lst.append((self.getLowerMatrix(M)+self.getLowerMatrix(M).transpose())/self.K(2))
            else:
               #pk_lst.append(M)
               #print 'char 0'
               #print type(M)
               pk_lst.append(self.getLowerMatrix(M))
        return pk_lst,S,T,sk_lst
    
    def encryptBlock(self,message,pk):
        """Get a ciphertext of 'message' using 'pk'
    
        INPUT:
    
        - 'message' - message to encrypt
        - 'pk' - public key
        
        OUTPUT:
        
        - 'c' - ciphertext
        - 'hashh' = digest of 'message'
    
        """
        c = vector(self.K,self.m)
        mm = hashlib.md5()
        mm.update(STS.binary2str(message))
        for i in range(self.m):
            c[i] = (message*pk[i]*message.column())[0]
        hashh = mm.digest()
        return c, hashh
    
    def verificar(self, v,M,y):
        """Using by decrypt to verify if 'M(v[0],v[1],...,v[n]) = y'
    
        INPUT:
    
        - 'v' - guess submessage
        - 'M' - part of public key
        - 'y' - part of ciphertext
    
        """
        boo = (y == (v*M*v.column())[0])
        return boo
    
    def decryptBlock(self, c1, S,T,sk_maps):
        """Get a guess original 'message' using secret key
    
        INPUT:
    
        - 'c1' - ciphertext
        - 'S,T,sk_maps' - private key
        
        OUTPUT:
        
        - 'message1' - guess original message
        """
        c1 = T.inverse()*c1
        randomVector = vector(self.K,self.m)
        for l in range(self.L):
            #print '::::::::::::::',l,':::::::::::::::::::'
            band = True
            count = 0
            while(count<self.r):
                for j in range(l*self.r,(l+1)*self.r):
                    randomVector[j] = self.K.random_element()
                for i in range(l*self.r,(l+1)*self.r): #sirve para verificar si un vector aleatorio v[l*r] satifaz sk_maps[i]
                    #print i
                    #if verificar(randomVector[0:((l+1)*r)],sk_maps[i][0:(l+1)*r,0:(l+1)*r],c1[0:(l+1)*r][i]) == False:
                    if self.verificar(randomVector,sk_maps[i],c1[i]) == False:
                        count = 0
                        break
                    else: 
                        #print 'entro count',count
                        count = count + 1
        message1 = S.inverse()*randomVector
        return message1
    
    def decryptBlock1(self, c1,S1,T1,sk_maps1):
        """Get a guess original 'message' using secret key
    
        INPUT:
    
        - 'c1' - ciphertext
        - 'S,T,sk_maps' - private key
        
        OUTPUT:
        
        - 'message1' - guess original message
        """
        c1 = T1.inverse()*c1
        randomVector = vector(self.K,self.m)
        for l in range(self.L):
            #print '::::::::::::::',l,':::::::::::::::::::'
            band = True
            count = 0
            while(count<self.r):
                for j in range(l*self.r,(l+1)*self.r):
                    randomVector[j] = self.K.random_element()
                for i in range(l*self.r,(l+1)*self.r): #sirve para verificar si un vector aleatorio v[l*r] satifaz sk_maps[i]
                    #print i
                    if self.verificar(randomVector[0:((l+1)*self.r)],sk_maps1[i][0:(l+1)*self.r,0:(l+1)*self.r],c1[0:(l+1)*self.r][i]) == False:
                        count = 0
                        break
                    else: 
                        #print 'entro count',count
                        count = count + 1
        message1 = S1.inverse()*randomVector
        return message1
    
    ######################################## High Rank Attack ###########################################
    def substractionSpaces(self, L,L1):
        """Get a vector space L1.intersection(L.complement())
        """
        return L1.intersection(L.complement())
    
    def matrixCheck(self,pkLst,v,l):
        """Check if sum(v[i]*pkLst[i]) has rank <= l*r
        
        INPUT:
    
        - 'pkLst' - public matrix polynomials
        - 'v' - random vector
        - 'l' - layer
        """
        sum = matrix(self.K,self.m,self.m)
        for i in range(self.m):
            sum = sum + v[i]*pkLst[i]
        return sum.rank() <= l*self.r
    
    
    def minRank1(self, pkLst):
        """Get the equivalent of T, T~
        
        INPUT:
    
        - 'pkLst' - public matrix polynomials
    
        OUTPUT:
        
        - 'spacesKernel' - kernel Kl to 1<=l<=n
        - 'Tm1.inverse()' - equivalent of T, T~
        """
        Kl = VectorSpace(self.K,self.m)
        #Jl = VectorSpace(K,K(0))
        Jl = span(self.K,[vector(self.K,self.m)])
        Tm1 = matrix(self.K,self.m,self.m)
        for l in range(self.L):
            #print '::::::::::::::',l,':::::::::::::::;'
            l1 = self.L-l-1
            while True:
                w = Kl.random_element()
                MS = Matrix(self.K,self.m,self.m)
                for k in range(self.n):
                    for r1 in range(self.m):
                        MS[k,r1] = sum((pkLst[r1])[k,j]*w[j] for j in range(self.n))
                Jl1 = MS.transpose().kernel()
                #print "Jl1",Jl1
                #print "Jl",Jl
                if (Jl1.dimension() == (l+1)*self.r):
                    #print "In",Kl.intersection(Kl1.complement())
                    Jt = self.substractionSpaces(Jl,Jl1)
    		#print "Subs",Kt
    		Jl = Jl1
    		flag = 0
    		for i in range(self.r):
    		    #print ':i = ',i,':::::::::::::::;' 
    		    t = Jt.basis_matrix()[i]
                        #Tm1[(l1+1)*r-i-1] = t
    		    Tm1[l*self.r+i] = t
                   
                    Kl = span(self.K,sum(t[j]*pkLst[j] for j in range(self.m)))
                    break
        return 0,Tm1.inverse()
    
    def minRank(self,pkLst):
        """Get the equivalent of T, T~
        
        INPUT:
    
        - 'pkLst' - public matrix polynomials
    
        OUTPUT:
        
        - 'spacesKernel' - kernel Kl to 1<=l<=n
        - 'Tm1.inverse()' - equivalent of T, T~
        """
        Kl = VectorSpace(self.K,self.m)
        J0 = VectorSpace(self.K,0)
        Tm1 = matrix(self.K,self.m,self.m)
        for l in range(self.L):
            print '::::::::::::::',l,':::::::::::::::;'
            l1 = self.L-l-1
            while True:
                w = Kl.random_element()
                MS = Matrix(self.K,self.m,self.m)
                for k in range(self.n):
                    for r1 in range(self.m):
                        MS[k,r1] = sum((pkLst[r1])[k,j]*w[j] for j in range(self.n))
                Kl1 = MS.transpose().kernel()
                #print "Hola",Kl1
                if (Kl1.dimension() == self.m-(l+1)*self.r):
                    #print "In",Kl.intersection(Kl1.complement())
                    Kt = self.substractionSpaces(Kl1,Kl)
    		#print "Subs",Kt
    		Kl = Kl1
    		flag = 0
    		for i in range(self.r):
    		    #print ':i = ',i,':::::::::::::::;' 
    		    t = Kt.basis_matrix()[i]
                Tm1[(l1+1)*self.r-i-1] = t
    		    #Tm1[l*r+i] = t
                break
        return 0,Tm1.inverse()
                    
    
         
    def highRank(self, pkLst):
        """Get the equivalent of T, T~
        
        INPUT:
    
        - 'pkLst' - public matrix polynomials
    
        OUTPUT:
        
        - 'spacesKernel' - kernel Kl to 1<=l<=n
        - 'Tm1.inverse()' - equivalent of T, T~
        """
        while True:
            Jl1 = VectorSpace(self.K,self.m)
    	print ":>>>>>><<<:"
    	spacesKernels = []
    	spacesKernels.append(span(self.K,matrix(self.K,self.m,self.m)))
    	spacesP = []
    	Tm1 = matrix(self.K,self.m,self.m)
    	for l in range(self.L):
    	    Jlvectors = []
    	    l1 = self.L-l-1
    	    #print '::::::::::::::',l,':::::::::::::::;'
    	    while (True):
    	        v = Jl1.random_element() # v \in J_{l+1}
    		if self.matrixCheck(pkLst,v,l1)==True:
    		    #print 'v=',v
    		    Jlvectors.append(v)
    		    Jl = span(self.K,Jlvectors)
    		    if Jl.dimension() == (l1)*self.r:
    		        #print 'Jl interserct JL1',Jl1.intersection(Jl.complement())
    		        Jt = self.substractionSpaces(Jl,Jl1)
    		        #print Jt
    		        Jl1 = Jl
    		        flag = 0
    		        for i in range(self.r):
    		            #print ':i = ',i,':::::::::::::::;' 
    		            t = Jt.basis_matrix()[i]
    		            Tm1[(l1+1)*self.r-i-1] = t
    		            #Tm1[(l1+1)*r-r+i] = t
    		            tPk = sum(t[ii]*pkLst[ii] for ii in range(self.m))
    		            tAux = t
    		            #print 'tpk antes',tPk.rank()
    		            '''
    		            #The two if are to get spaces Plr with dimension == lr
    		            if tPk.rank() == (l)*r and flag == 0:
    		                print "Entro if 1"
    		                spacesKernels.append(tPk)
    		                flag = 1
    		            if i == r-1 and flag == 0:
    		                print "Entro if 2"
    		                spanAux = span(K,tPk)
    		                while(true):
    		                    tAux = spanAux.random_element()
    		                    tPk = sum(tAux[ii]*pkLst[ii] for ii in range(m))
    		                    if tPk.rank() == (l)*r:
    		                        spacesKernels.append(tPk)
    		                        break
    		            print 'tAux', tAux
    		            print tPk
    		            '''
    		            spacesP.append((self.getLowerMatrix(tPk)+self.getLowerMatrix(tPk).transpose())/self.K(2))
    		        #spacesKernels.append(tPk)
    		        #print Jl.basis_matrix().kernel()
    		        spacesKernels.append(Jl.complement())
    		        break
            #print "l1:", l1 
            if Tm1.rank() ==self.m:
    	        break
        spacesP.reverse()
        spacesKernels.reverse()         
        return spacesKernels,Tm1.inverse(),spacesP
    
    def inversionAttack1(self, P,TT,y):
        pk_lst1 = []
        for i in range(self.m):
            M = matrix(self.K,self.m,self.m)
            for j in range(self.m):
                M = M + TT.inverse()[i,j]*P[j]
            pk_lst1.append((self.getLowerMatrix(M)+self.getLowerMatrix(M).transpose())/self.K(2))
            #pk_lst1.append(M)
        
        c1 = TT.inverse()*y
        
        randomVector = vector(self.K,self.m)
        for l in range(self.L):
            #print '::::::::::::::',l,':::::::::::::::::::'
            band = True
            count = 0
            while(count<self.r):
                for j in range(l*self.r,(l+1)*self.r):
                    randomVector[j] = self.K.random_element()
                for i in range(l*self.r,(l+1)*self.r): #sirve para verificar si un vector aleatorio v[l*r] satifaz sk_maps[i]
                    #print i
                    #if verificar(randomVector[0:((l+1)*r)],sk_maps[i][0:(l+1)*r,0:(l+1)*r],c1[0:(l+1)*r][i]) == False:
                    if self.verificar(randomVector,pk_lst1[i],c1[i]) == False:
                        count = 0
                        break
                    else: 
                        #print 'entro count',count
                        count = count + 1
        
        return randomVector
        
    def inversionAttack(self, P,TT,listSpaceKernels,y):
        pk_lst1 = []
        for i in range(self.m):
            M = matrix(self.K,self.m,self.m)
            for j in range(self.m):
                M = M + TT.inverse()[i,j]*P[j]
            pk_lst1.append((self.getLowerMatrix(M)+self.getLowerMatrix(M).transpose())/self.K(2))
            #pk_lst1.append(M/K(2))
        yy = y*TT.inverse()
        #listSpaceKernels.append(span(K,[vector(K,m)]))
        return self.recursivePart(pk_lst1,vector(self.K,self.m),1,listSpaceKernels,yy)
    
    def recursivePart(self, P,x,l,listSpaceKernels,y):
        print 'l:',l,'\n'
        Xlist = []
        if( l > self.L):
            Xlist.append(x)
            return Xlist
        #Kt = listSpaceKernels[l-1].intersection(listSpaceKernels[l].complement())
        Kt = listSpaceKernels[l].intersection(listSpaceKernels[l-1].complement())
        #print Kt
        
        for j in range(len(Kt)):
            count = 0
            for i in range((l-1)*self.r,l*self.r): #sirve para verificar si un vector aleatorio v[l*r] satifaz sk_maps[i]
                #print 'y[0:(l+1)*r][i]', y[0:(l+1)*r][i]
                if self.verificar((x+Kt[j]),P[i],y[i]) == False:
                    count = 0
                    break
                else:    
                    count = count + 1
            #print count
            if count!=0:
                Xlist.append(self.recursivePart(P,x+Kt[j],l+1,listSpaceKernels,y))
        return Xlist
    
    def rndKernel(self, matSystem, selectSpace, curLayer):
        curCnt = 0;
        while True:
            while True:
                rndVec = selectSpace.random_element();
                guessMat = matrix(self.K, [rndVec*matSystem[i]  for i in range(len(matSystem))])
                #printf "guess Mat %o, select Space %o", guessMat,;
                lambdaSpace = guessMat.kernel()
                curCnt = curCnt + 1;
                if curCnt >= 200:
                    print "reset\n"
                    reset()
                if (lambdaSpace.dimension() >= 1) and (lambdaSpace.dimension() <= self.r*curLayer):
                    break
            resKernel = selectSpace
            tryMat = matrix(self.K,self.m,self.m)
            for i in range(1,100):
               lambdaSeq = lambdaSpace.random_element()
               tryMat = sum(lambdaSeq[j]*pk_lst[j] for j in range(self.m))
               resKernel= resKernel.intersection(tryMat.kernel())
            if (resKernel.dimension() >= selectSpace.dimension()-self.r-2):
                break
        return resKernel
    
    def getKernelSpaces(self):
        curSelSpace = VectorSpace(self.K,self.m);
        curLayer = 1;
        KernelSpaces = []
        KernelSpaces.append(curSelSpace)
        while True:
            curCnt = 0;
            KernelMatrix = matrix(self.K,self.m,self.m);
            while True:
        	    smallKernel = self.rndKernel(matSeq, curSelSpace, curLayer).intersection(self.rndKernel(matSeq, curSelSpace, curLayer));
        	    for i in range(smallKernel.dimension()):
                        KernelMatrix[KernelMatrix.rank()] = smallKernel.basis_matrix()[i]
        	    curCnt = curCnt + 1;
        	    if (KernelMatrix.rank() >= self.n-(curLayer*self.r)) or (curCnt >= 200):
                        break
    	while KernelMatrix.rank() > self.m-(curLayer*self.r):
                #print "random", [kk for kk in range(1,KernelMatrix.rank())], random.choice([kk for kk in range(1,KernelMatrix.rank())])
                KernelMatrix[random.choice([kk for kk in range(0,KernelMatrix.rank())])] = vector(self.K,self.m)
            	#print "\nNew Layer: ", curLayer,"\n"
            	KernelSpaces.append(span(self.K,KernelMatrix))
            	curSelSpace = span(self.K,KernelMatrix)
            	curLayer = curLayer + 1;
                if (curLayer >= self.L) or (curCnt >= 200):
                    break;
        return KernelSpaces
    def printVarValue(self,var1,value1):
        if self.modePrint == 0:
            var("bps", latex_name=var1+"="+latex(value1))
            show(bps)
        else:
            print value1

    def rep(self,ele):
        if self.pot == 1:
            return str(ele)
        else:
            return str(ele.int_repr())

    #def printPublicKey_ToolANF(self, pk, c, v, seg, iter): #falta arrumar v             
    def printPublicKey_ToolANF(self, pk, c, v, iter): #falta arrumar v 
        #f = open("/var/www/html/mq/sts_"+str(self.L)+"_"+str(self.r)+"_"+str(self.pot)+"_"+str(seg),'w')    
        #f = open(path_output+"sts_"+str(self.L)+"_"+str(self.r)+"_"+str(self.pot)+"_"+str(seg)+"_"+str(iter),'w')   
        
	f = open(path_output+"sts_"+str(self.L)+"_"+str(self.r)+"_"+str(self.pot)+"_"+str(iter)+".mq",'w')   
        print str(self.m) + " " + str(self.n) + " " + str(self.K.modulus().degree())
        f.write(str(self.m) + " " + str(self.n) + " " + str(self.K.modulus().degree())+"\n")
        linha = ""
        for i in range(self.m):
            linha = linha + self.rep(c[i]) + " "
        #print linha
        f.write(linha+"\n")
        #print 'pk=', pk
        for k in range(self.m):
            for i in range(self.n):
                #print "i:", i
                linha = ""
                for j in range(i+1):
                    #print j
                    linha = linha + self.rep(pk[k][i][j]) + " "
                #print 'linha', linha
                f.write(linha+"\n")
            f.write(v+"\n")
            #print "\n"
        f.close()


#sts = STS(2, 2, 2, 3)
#sts.modePrint = 1  
#k_lst = sts.keyGeneration()
#pk_lst = k_lst[0]
#pretty_print(pk_lst)
#S = k_lst[1]

#T = k_lst[2]
#sk_lst = k_lst[3]
#pretty_print("::::::::::::::::::::::::::::: Chave Privada :::::::::::::::::::::::")
#sts.printVarValue("T",T)
#sts.printVarValue("P'",sk_lst)
#sts.printVarValue("S",S)
#pretty_print("::::::::::::::::::::::::::::: Chave Publica :::::::::::::::::::::::")
#sts.printVarValue("P",pk_lst)
#message = vector([sts.K.random_element() for _ in range(sts.n)])
#sts.printVarValue("msg",message)
#c = sts.encryptBlock(message, pk_lst)

#print sts.decryptBlock(c[0], S, T, sk_lst)
#print 'message',message

#TT = sts.minRank1(pk_lst)
#print 'found'
#print sts.inversionAttack1(pk_lst,TT,c[0])

