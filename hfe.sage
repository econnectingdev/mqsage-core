import sys
from sage.all import *
from sage.sat.converters.polybori import CNFEncoder
from sage.sat.solvers.dimacs import DIMACS
from sage.crypto.mq.sbox import SBox
from sage.crypto.mq.mpolynomialsystemgenerator import MPolynomialSystemGenerator
from sage.sat.boolean_polynomials import solve as solve_sat
#from sage.crypto.mq.mpolynomialsystem import MPolynomialSystem, MPolynomialRoundSystem
from sage.rings.polynomial.multi_polynomial_sequence import PolynomialSequence
from sage.rings.polynomial import *
#from sage.misc.functional import parent
from sage.misc.cachefunc import cached_function
from sage.modules.free_module import VectorSpace, FreeModule

from sage.rings.all import FiniteField as GF
from sage.rings.polynomial.polynomial_ring_constructor import BooleanPolynomialRing_constructor as BooleanPolynomialRing
from sage.rings.polynomial.pbori import BooleanPolynomialRing as BooleanPolynomialRing_class

from abc import *
import hashlib
import random
import binascii
import string

class HFE:
    def __init__(self, r, pot, q):
        print "HFE"
        self.modePrint = 1
        self.r = int(r)
        self.q = int(q)
        self.pot = int(pot)
        self.K = GF(q**pot,"t")
        self.PR = PolynomialRing(self.K,"X")
        self.VS = self.K.vector_space()
        self.car = 2 if q%2==0 else 1
        self.m = 1
        self.n = self.pot
        
    def privKeyGen(self):
        P = matrix(self.K,self.pot,self.pot)
        R = random_matrix(self.K,self.r,self.r)
        P[0:self.r,0:self.r] = R
        S = self.PR.random_element(degree=self.pot-1)
        Slist = S.list()
        T = self.PR.random_element(degree=self.pot-1)
        Tlist = T.list()
        return P,Tlist,Slist
    
    def pubKeyGen(self,P,Tlist,Slist):
        W = matrix(self.K,self.pot,self.pot)
        for i in range(self.pot):
            for j in range(self.pot):
                W[i,j] = (Slist[mod(i-j,self.pot)]**(self.q**i))
        v = self.VS.random_element()
        temp = W*P*transpose(W)
        pk = matrix(self.K,self.pot,self.pot)
        for k in range(self.pot):
            S = matrix(self.K,self.pot,self.pot)
            for i in range(self.pot):
                for j in range(self.pot):
                    S[i,j] = temp[mod(i-k,self.pot),mod(j-k,self.pot)]**(self.q**k)
            pk = pk + Tlist[k]*S
        return self.getLowerMatrix(pk)#modify by Juan 23/03/2016
    
    def keyGeneration(self):
        priv = self.privKeyGen()
        pk = self.pubKeyGen(priv[0],priv[1],priv[2])
        return pk
        
    def getLowerMatrix(self,M):
        """Convert matrix representation of quadratic polynomial in lower triangular matrix.
    
        INPUT:
    
        - 'M' - matrix on K
        """
        for i in range(M.nrows()):
            for j in range(M.ncols()):
                if (i>j):
                    M[i,j] = M[i,j] + M[j,i]
                    M[j,i] = 0
        return M
   
    def rep(self,ele):
        if self.pot == 1:
            return str(ele)
        else:
            return str(ele.int_repr())

    #def printPublicKey_ToolANF(self, pk, c, v, seg, iter): #falta arrumar v 
    def printPublicKey_ToolANF(self, pk, c, v, iter): #falta arrumar v 
        #f = open("/var/www/html/mq/hfe_"+str(self.r)+"_"+str(self.pot)+"_"+str(seg),'w')    
        #f = open(path_output+"hfe_"+str(self.r)+"_"+str(self.pot)+"_"+str(seg)+"_"+str(iter),'w')
        f = open(path_output+"hfe_"+str(self.r)+"_"+str(self.pot)+"_"+str(iter)+".mq",'w')  
        print str(self.m) + " " + str(self.n) + " " + str(self.K.modulus().degree())
        f.write(str(self.m) + " " + str(self.n) + " " + str(self.K.modulus().degree())+"\n")
        linha = ""
        for i in range(self.m):
            linha = linha + c[i].int_repr() + " "
        print linha
        f.write(linha+"\n")
        for k in range(self.m):           
            for i in range(self.n):
                linha = ""
                for j in range(i+1):
                    #print 'i,j', i,j, pk[i][j]
                    linha = linha + str(self.rep(pk[i][j])) + " "
                print linha
                f.write(linha+"\n")
            f.write(v+"\n")
            #print "\n"
        f.close()
