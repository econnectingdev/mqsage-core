"""
ABC Implementation

AUTHOR: Juan Grados <juancgv@lncc.br>, Pedro Lara, Renato Portugal
"""
import sys
from sage.all import *
from sage.sat.converters.polybori import CNFEncoder
from sage.sat.solvers.dimacs import DIMACS
from sage.crypto.mq.sbox import SBox
from sage.crypto.mq.mpolynomialsystemgenerator import MPolynomialSystemGenerator
from sage.sat.boolean_polynomials import solve as solve_sat
#from sage.crypto.mq.mpolynomialsystem import MPolynomialSystem, MPolynomialRoundSystem
from sage.rings.polynomial.multi_polynomial_sequence import PolynomialSequence
from sage.rings.polynomial import *
#from sage.misc.functional import parent
from sage.misc.cachefunc import cached_function
from sage.modules.free_module import VectorSpace, FreeModule

from sage.rings.all import FiniteField as GF
from sage.rings.integer_ring import ZZ
from sage.rings.polynomial.polynomial_ring_constructor import BooleanPolynomialRing_constructor as BooleanPolynomialRing
from sage.rings.polynomial.pbori import BooleanPolynomialRing as BooleanPolynomialRing_class
import hashlib
import random
import binascii
import string

class ABC:

    def __init__(self, q, s, pot):
        #print "ABC"
        self.q = q
        self.pot = pot
        self.s = s
        self.n = s**2
        self.m = 2*self.n
        kk = GF(q**pot,"t")
        self.kk = kk
        self.VS = kk.vector_space()
        self.car = 2 if q%2==0 else 1
        self.vars_GF = [var("X%d"%i) for i in range(self.n)]
        self.vars_gf_X_ = [var("X_%d"%i) for i in range(pot)]
        self.vars_gf_Y_ = [var("Y_%d"%i) for i in range(pot)]
        self.vars_gf_Z_ = [var("Z_%d"%i) for i in range(pot)]
        nsum = (self.n*(self.n+1))/2
        self.vars_a = [var("a%d"%i) for i in range(self.n)]
        #print "variables de los coefficientes:\n",self.vars_coeffs_gf
        self.vars_gf2 = [var("x%d%d"%(j,i)) for j in range(self.n) for i in range(pot)]
        self.P=PolynomialRing(self.kk,names=self.vars_GF)
        self.B = BooleanPolynomialRing(len(self.vars_gf2), self.vars_gf2)
        self.B1 = BooleanPolynomialRing(len(self.vars_gf_X_ + self.vars_gf_Y_ + self.vars_gf_Z_),self.vars_gf_X_ + self.vars_gf_Y_ + self.vars_gf_Z_)
        self.A = companion_matrix(self.kk.modulus())
        sum_X = 0
        sum_Y = 0
        sum_Z = 0
        for i in range(pot):
            sum_X = sum_X + (self.A**i)*self.vars_gf_X_[i]
            sum_Y = sum_Y + (self.A**i)*self.vars_gf_Y_[i]
            sum_Z = sum_Z + (self.A**i)*self.vars_gf_Z_[i]
        #print sum_X
        self.funcsPmul = []
        if self.pot == 2:
            self.modulus_GF2X = '07'
            Msum = sum_X + sum_Y
            #print "Msum", Msum
            Mmul = sum_X*sum_Y*sum_Z
  
            self.pmul0 = (Mmul)[0,0]
            self.pmul1 = (Mmul)[0,1]

            for i in range(2):
                self.funcsPmul.append((Mmul)[0,0])
                self.funcsPmul.append((Mmul)[0,1])


        if self.pot == 8:
            self.modulus_GF2X = '0435'
            Msum = sum_X + sum_Y
            #print "Msum", Msum
            Mmul = sum_X*sum_Y*sum_Z
            for i in range(8):
               
                self.fsum = lambda xx: [self.B1((Msum)[0,0])(*xx),self.B1((Msum)[1,0])(*xx),self.B1((Msum)[2,0])(*xx),self.B1((Msum)[3,0])(*xx),self.B1((Msum)[4,0])(*xx),self.B1((Msum)[5,0])(*xx),self.B1((Msum)[6,0])(*xx),self.B1((Msum)[7,0])(*xx)]
                self.funcsPmul.append(self.B1((Mmul)[0,0]))
                self.funcsPmul.append(self.B1((Mmul)[1,0]))
                self.funcsPmul.append(self.B1((Mmul)[2,0]))
                self.funcsPmul.append(self.B1((Mmul)[3,0]))
                self.funcsPmul.append(self.B1((Mmul)[4,0]))
                self.funcsPmul.append(self.B1((Mmul)[5,0]))
                self.funcsPmul.append(self.B1((Mmul)[6,0]))
                self.funcsPmul.append(self.B1((Mmul)[7,0]))   

            self.fmulbari = lambda xx: [substitute_variables(self.B1,xx,self.funcsPmul[0]),substitute_variables(self.B1,xx,self.funcsPmul[1]),substitute_variables(self.B1,xx,self.funcsPmul[2]),substitute_variables(self.B1,xx,self.funcsPmul[3]),substitute_variables(self.B1,xx,self.funcsPmul[4]), substitute_variables(self.B1,xx,self.funcsPmul[5]),substitute_variables(self.B1,xx,self.funcsPmul[6]), substitute_variables(self.B1,xx,self.funcsPmul[7])]
      

    def keyGeneration(self):
        B = random_matrix(self.kk, self.s, self.s*self.n, sparse=True,density=0.9)
        C = random_matrix(self.kk, self.s, self.s*self.n, sparse=True,density=0.9)
        #generating L1 e L2
        while(true):
            L10 = matrix(self.kk,self.s,self.s)
            L1 = random_matrix(self.kk, self.n, self.n,sparse=True,density=0.9)
            L1[0:self.s,0:self.s] = L10
            L2 = random_matrix(self.kk, self.m, self.m, sparse=True,density=0.9)
            if (L1.rank() == self.n and L2.rank() == self.m):
                break
        Prs = []
        self.genPublicMatrix(Prs,B,C,L1,L2)
        return Prs,L1,L2,B,C

    def genPublicMatrix(self,Prs,B,C,L1,L2):
        lst = []
        for l in range(self.s):
            for k in range(self.s):
                ii = l*self.s
                B1 = B[0:self.s,k*self.n:k*self.n+self.n]
                f = Matrix(self.kk, self.n, self.n)
                for i in range(self.s):
                    for j in range(self.n):
                        if (B1[i,j]):
                            f[ii,j] = B1[i,j]
                    ii = ii+1
                if self.car == 1:
                    lst.append((ABC.getLowerMatrix(f)+ABC.getLowerMatrix(f).transpose())/self.kk(2))
                else:
                    lst.append((f))
        for l in range(self.s):
            for k in range(self.s):
                ii = l*self.s
                B1 = C[0:self.s,k*self.n:k*self.n+self.n]
                f = Matrix(self.kk, self.n, self.n)
                for i in range(self.s):
                    for j in range(self.n):
                        if (B1[i,j]):
                            f[ii,j] = B1[i,j]
                    ii = ii+1
                if self.car == 1:
                    lst.append((ABC.getLowerMatrix(f)+ABC.getLowerMatrix(f).transpose())/self.kk(2))
                else:
                    lst.append((f))
        lst_temp = []
        L1_trans = transpose(L1)
        for j in range(self.m):
            lst_temp.append(L1_trans*lst[j]*L1)
        for i in range(self.m):
            sum = matrix(self.kk,self.n,self.n)
            for j in range(self.m):
                sum = sum + L2[i,j]*lst_temp[j]
            if self.car == 1:
                Prs.append((ABC.getLowerMatrix(matrix(self.kk,sum))+ABC.getLowerMatrix(matrix(self.kk,sum)).transpose())/self.kk(2))
            else:
                Prs.append(ABC.getLowerMatrix(matrix(self.kk,sum)))

    def getPubPolynomials(self, Pub):
        polys = []
        for i in range(len(Pub)):
            polys.append(self.P(self.matrix2Polynomial(Pub[i])))
        return polys

    def matrix2Polynomial(self, M):
        x = vector(self.vars_GF)
        return x*M*x.column()
     
    def preparingArgs(self, element, i , j):
         #sirven para armar los argumentos
         lst_arg = []
         for k in range(3*self.pot):
            if (k <self.pot):
                if self.VS(element)[k%self.pot] == 0:
                    lst_arg.append(self.B.zero_element())
                else:
                    lst_arg.append(self.B.one_element())
            elif (k >= self.pot and k<2*self.pot):
                lst_arg.append(self.B.gens()[i*pot + (k%self.pot)])
            elif (k >= 2*self.pot and k<3*self.pot):
                lst_arg.append(self.B.gens()[j*pot + (k%self.pot)])
         #print lst_arg
         return lst_arg

    def getCNFPolynomial(self, pub, c):               
        for l in range(len(pub)):
            print "l:",l
            p = pub[l]
            x = self.preparingArgs(p[0,0], 0, 0)
            componentes = self.fmulbari(x)
            for i in range(self.n):
                for j in range(i+1):
                     if i != 0 or j !=0:
                         x  = (self.preparingArgs(p[i,j], i,j))
                         temp = self.fmulbari(x)
                         componentes = tuple(map(lambda (x, y): x + y, zip(tuple(componentes), tuple(temp))))
            #print componentes

    @staticmethod
    def getLowerMatrix(M):
        '''Convert matrix representation of quadratic polynomial in lower triangular matrix.
    
        INPUT:
    
        - 'M' - matrix on K
        '''
        
        for i in range(M.nrows()):
            for j in range(M.ncols()):
                if (i>j):
                    M[i,j] = M[i,j] + M[j,i]
                    M[j,i] = 0
        return M

    @staticmethod
    def binary2str(message):
        s = ""
        for i in range(len(message)):
            s = s + str(message[i])
        return s

    def encryptBlock(self, pk, message):
        c = vector(self.kk, self.m)
        mm = hashlib.md5()
        mm.update(ABC.binary2str(message))
        for i in range(self.m):
            c[i] = (message*pk[i]*message.column())[0]
        return c,mm.digest()

    def fillM(self, E12, B, C): 
        if B!=None:
            M1 = matrix(self.kk,self.n,self.n)
        else:
            M1 = matrix(self.kk,self.n,self.m)
            for i in range(self.s):
                M1[i*self.s:i*self.s+self.s,self.n+i*self.s:self.n+i*self.s+self.s] = transpose(E12)
        ii = 0
        for i in range(self.s):         
            for j in range(self.s):
                if B!=None:
                    self.fillMatrixCoefficients(M1,ii,i,j,E12,B,C)
                else:
                    self.fillMatrixCoefficients1(M1,ii,i,j,E12,B,C)
                ii = ii + 1
        return M1
    def fillMatrixCoefficients(self, M, ii, i, j, E12, B, C):
        for k in range(self.n):
            sum = 0 
            for r in range(self.s):
                sum = sum + self.kk(((B[i:i+1,r*self.n:r*self.n+self.n])[0][k])*E12[r,j])
            M[ii,k] = sum - (C[i:i+1,j*self.n:j*self.n+self.n])[0][k]
            
    def fillMatrixCoefficients1(self, M ,ii ,i ,j ,E12 ,B ,C):
        for k in range(self.n):
                M[ii,k] = ((C[i:i+1,j*self.n:j*self.n+self.n])[0][k%self.n])
    
    def decryptBlock(self, c, hashu, L1, L2 ,B ,C, message):
        Bl = matrix([[sum(B[j,k]*(self.vars_GF[k%self.n]) for k in range(i*self.n,i*self.n+self.n)) for i in range(self.s)] for j in range(self.s)])
        
        #C = lst[4]
        Cl = matrix([[sum(C[j,k]*(self.vars_GF[k%self.n]) for k in range(i*self.n,i*self.n+self.n)) for i in range(self.s)] for j in range(self.s)])
        Al = matrix([[self.vars_a[i*self.s+j] for i in range(self.s)] for j in range(self.s)])
        ff = 0
        #print 'L2.inverse()*c\n'
        c = L2.inverse()*c
        var("bps", latex_name=r"y="+latex(c))
        #show(bps)
        E1 = matrix(self.kk,self.s,self.s)
        E2 = matrix(self.kk,self.s,self.s)
        ii = 0
        jj = 0
        #filling E1
        for i in range(self.n):
            resul = c[i]
            #print resul
            if (i % self.s  == 0 and i > 0):
                ii = ii + 1
            #print ii,jj
            E1[ii,jj] = resul
            jj = jj + 1
            if jj % self.s == 0:
                jj = 0
        #filling E2
        ii = 0
        jj = 0
        for i in range(self.n,self.n+self.n):
            resul = c[i]
            if (i % self.s  == 0 and i > self.n):
                ii = ii + 1
            E2[ii,jj] = resul
            jj = jj + 1
            if jj % self.s == 0:
                jj = 0
        E1l = var('E1')
        E2l = var('E2')
        """
        var("bps1", latex_name=r"E1="+latex(E1))
        show(bps1)
        var("bps", latex_name=r"E2="+latex(E2))
        show(bps)
        """
        if E1.determinant()!=0:
            #print 'CASE I'
            E12 = E1.inverse()*E2
            #print 'B*E1.inverse()*E2=C\n'
            #var("bps1", latex_name=r""+latex(Bl)+latex(E12)+"="+latex(Cl))
            #show(bps1)
            #var("bps2", latex_name=r""+latex(Bl*E12-Cl)+"="+latex(matrix(self.s,self.s)))
            #show(bps2)
            MM = Bl*E12-Cl
            #print 'coeffs',MM[0,0].coefficient(x1)
            M = self.fillM(E12,B,C)
        if E2.determinant()!=0 and E1.determinant()==0:
            #print 'CASE II'
            E21 = E2.inverse()*E1
            #print 'C*E2.inverse()*E1 = B\n'
    
            #var("bps1", latex_name=r""+latex(Cl)+latex(E21)+"="+latex(Bl))
            #show(bps1)
            #var("bps2", latex_name=r""+latex(Cl*E21-Bl)+"="+latex(matrix(self.s,self.s)))
            #show(bps2)
    
            M = self.fillM(E21,C,B)        
        if E1.determinant() == 0 and E2.determinant() == 0:
            ff = 1
            #print 'CASE III'
            #print 'A.inverse()E1=B and A.inverse()E2=C'
            A = None
            M = matrix(self.kk,self.m,self.m)
            M1 = self.fillM(E1,A,B)
            M2 = self.fillM(E2,A,C)
            M[0:self.n,0:self.m] = M1
            M[self.n:self.m,0:self.m] = M2
            var("bps", latex_name=r""+latex(M)+latex(vector([self.vars_GF[i] for i in range(self.n)]+[self.vars_a[i] for i in range(self.n)]).column())+"="+latex(vector([0 for i in range(self.m)]).column()))
            #show(bps)   
        count = 0
        if ff == 0:
            var("bps", latex_name=r""+latex(M)+latex(vector([self.vars_GF[i] for i in range(self.n)]).column())+"="+latex(vector([0 for i in range(self.n)]).column()))
            #show(bps)
        while(count < 2):
            VS = transpose(M).kernel()
            if ff==0:
                #print 'VS',VS
                cc = VS.coordinates(L1*message)
                x = matrix(self.kk,VS.basis_matrix().transpose()) * vector(self.kk,cc)
                #print x
            else:
                x = L1*message
                xM = Matrix(self.kk,self.s,self.s)
                for i in range(self.s):
                    for j in range(self.s):
                        xM[i,j] = x[i*self.s+j]
                
                xI = xM.inverse()
                xIv = vector(self.kk,self.n)
                for i in range(self.s):
                    for j in range(self.s):
                        xIv[i*s+j] = xI[i,j]
                xx = vector(self.kk,self.m)
                for i in range(self.n):
                    xx[i] = x[i]
                for i in range(self.n,self.m):
                    xx[i] = xIv[i-self.n]
                print xx
                print VS.basis_matrix()
                cc = VS.coordinates(xx)
                x = matrix(self.kk,VS.basis_matrix().transpose()) * vector(self.kk,cc)
                print x
            mm = hashlib.md5()
            mm.update(ABC.binary2str((L1.inverse())*x[0:self.n]))
            if mm.digest() == hashu: 
                #print mm.digest(),'=?', hashu,'\n'          
                #print 'xp=', x
                #print 'L1.inv()xp = message=', (L1.inverse())*x[0:self.n] 
                return 1
            count = count + 1            
        return 0
    
    @staticmethod
    def verificar(v,M,y):
        '''Using by decrypt to verify if 'M(v[0],v[1],...,v[n]) = y'
    
        INPUT:
    
        - 'v' - guess submessage
        - 'M' - part of public key
        - 'y' - part of ciphertext
    
        '''
        boo = (y == (v*M*v.column())[0])
        return boo

    def rep(self,ele):
        if self.pot == 1:
            return str(ele)
        else:
            return str(ele.int_repr())
        
    #def printPublicKey_ToolANF(self, pk, c, v, seg, iter): #falta arrumar v 
    def printPublicKey_ToolANF(self, pk, c, v, iter): #falta arrumar v

        #f = open("/var/www/html/mq/abc_"+str(self.s)+"_"+str(self.pot)+"_"+str(seg)+"_"+str(iter)+"_curva.mq",'w')    
        f = open(path_output+"abc_"+str(self.s)+"_"+str(self.pot)+"_"+str(iter)+".mq",'w')   
        #print str(self.m) + " " + str(self.n) + " " + str(self.kk.modulus().degree())
        f.write(str(self.m) + " " + str(self.n) + " " + str(self.kk.modulus().degree())+"\n")
        linha = ""
        for i in range(self.m):
            linha = linha + self.rep(c[i]) + " "
        #print linha
        f.write(linha+"\n")
        for k in range(self.m):
            for i in range(self.n):
                #print "i:", i
                linha = ""
                for j in range(i+1):
                    #print j
                    linha = linha + str(self.rep(pk[k][i][j])) + " "
                #print linha
                f.write(linha+"\n")
            f.write(v+"\n")
            #print "\n"
        f.close()

    def inversionAttack1(self, P,TT,y):
        r = 2*self.s
        pk_lst1 = []
        L = (1/2)*self.s
        for i in range(self.n):
            M = matrix(self.kk,self.n,self.n)
            for j in range(m):
                M = M + TT.inverse()[i,j]*P[j]
            pk_lst1.append((ABC.getLowerMatrix(M)+ABC.getLowerMatrix(M).transpose())/self.kk(2))
        for i in range(self.n,2*self.n):
            M = matrix(self.kk,self.n,self.n)
            for j in range(self.m):
                M = M + TT.inverse()[i,j]*P[j]
            pk_lst1.append((ABC.getLowerMatrix(M)+ABC.getLowerMatrix(M).transpose())/self.kk(2))
            #pk_lst1.append(M)
        print "y:",y
        print "TT.inverse()\n",TT.inverse()
        c1 = TT.inverse()*y
        print "c1:", c1
        randomVector = vector(self.kk,self.n)
        for l in range(L):
            #print '::::::::::::::',l,':::::::::::::::::::'
            band = True
            count = 0
            while(count<r):
                for j in range(l*r,(l+1)*r):
                    randomVector[j] = self.kk.random_element()
                for i in range(l*r,(l+1)*r): #sirve para verificar si un vector aleatorio v[l*r] satifaz sk_maps[i]
                    if ABC.verificar(randomVector,pk_lst1[i],c1[i]) == False:
                        count = 0
                        break
                    else: 
                        #print 'entro count',count
                        count = count + 1
        return randomVector
"""
n = 4
#m = 2*n
pot = 2
q = 2

s = sqrt(n)
s = int(s)
abc = ABC(q,s,pot)
lst = abc.keyGeneration()
pk = lst[0]
L1 = lst[1]
L2 = lst[2]
B = lst[3]
C = lst[4]

print '::::::::::::::::::::Public Key::::::::::::::::::\n'
pretty_print(pk)
print '::::::::::::::::::::Private Key::::::::::::::::::;:::'
print ':::::::::::::::::::::::::L1:::::::::::::::::::::::::\n'
pretty_print(L1)
print ':::::::::::::::::::::::::L2:::::::::::::::::::::::::\n'
pretty_print(L2)
print ':::::::::::::::::::::::::B:::::::::::::::::::::::::\n'
pretty_print(B)
print ':::::::::::::::::::::::::C:::::::::::::::::::::::::\n'
pretty_print(C)

#polysPub = abc.getPubPolynomials(pk)
message = vector([abc.kk.random_element() for _ in range(s**2)])
#message = vector(abc.kk,[0,0,1,0])
print message
c = abc.encryptBlock(pk, message)
#print ':::::::::::::::::Encrypt Message::::::::::::::'
#pretty_print(c[0])
hash_u = c[1]
#print "Obtendo polys"
#abc.getCNFPolynomial(pk,c[0])
#print abc.decryptBlock(c[0],hash_u,L1,L2,B,C)
abc.printPublicKey_ToolANF(pk,c[0])	
"""
