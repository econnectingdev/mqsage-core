# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
import sys
import traceback
from sage.all import *
from sage.sat.converters.polybori import CNFEncoder
from sage.sat.solvers.dimacs import DIMACS
from sage.crypto.mq.sbox import SBox
from sage.crypto.mq.mpolynomialsystemgenerator import MPolynomialSystemGenerator
from sage.sat.boolean_polynomials import solve as solve_sat
#from sage.crypto.mq.mpolynomialsystem import MPolynomialSystem, MPolynomialRoundSystem
from sage.rings.polynomial.multi_polynomial_sequence import PolynomialSequence
from sage.rings.polynomial import *
#from sage.misc.functional import parent
from sage.misc.cachefunc import cached_function
from sage.modules.free_module import VectorSpace, FreeModule

from sage.rings.all import FiniteField as GF
#from sage.rings.integer_ring import ZZ
from sage.rings.polynomial.polynomial_ring_constructor import BooleanPolynomialRing_constructor as BooleanPolynomialRing
from sage.rings.polynomial.pbori import BooleanPolynomialRing as BooleanPolynomialRing_class
root_path = "/home/grados-sanchez/grive/LNCC/doutorado/projects/MPQCSAT/scripts/mq/"
load(root_path+"abc.sage")
load(root_path+"sts.sage")
load(root_path+"hfe.sage")
load(root_path+"uov.sage")
from abc import *
#from sts import *
#from hfe import *
import hashlib
import random
import binascii
import string

class MQWriter:
    def __init__(self,lst_mqs):
        for mq in lst_mqs:
            if mq[0] == "abc":
                while True:
                    try:
                        s = mq[1]
                        q = mq[2]
			pot = mq[3]
                        abc = ABC(q,s,pot)
                        lst = abc.keyGeneration()
                        pk = lst[0]
                        L1 = lst[1]
                        L2 = lst[2]
                        B = lst[3]
                        C = lst[4]
                        
                        '''
                        print '::::::::::::::::::::Public Key::::::::::::::::::\n'
                        pretty_print(pk)
                         
                        print '::::::::::::::::::::Private Key::::::::::::::::::;:::'
                        print ':::::::::::::::::::::::::L1:::::::::::::::::::::::::\n'
                        pretty_print(L1)
                        print ':::::::::::::::::::::::::L2:::::::::::::::::::::::::\n'
                        pretty_print(L2)
                        print ':::::::::::::::::::::::::B:::::::::::::::::::::::::\n'
                        pretty_print(B)
                        print ':::::::::::::::::::::::::C:::::::::::::::::::::::::\n'
                        pretty_print(C)
                        '''
                        #polysPub = abc.getPubPolynomials(pk)
                        message = vector([abc.kk.random_element() for _ in range(s**2)])
                        #message = vector(abc.kk,[0,0,1,0])
                        #print 'plaintext'
                        #pretty_print(message)
                        c = abc.encryptBlock(pk, message)
                        #print 'ciphertext'
                        #pretty_print(c);
                        #print "Init Mq Writer"
                        pedro = ""
                        for i in range(abc.n+1):
                            pedro = pedro + "0 "
                        #bit level of attack
                        security_level = int(log(1.0*((q**(4*s))*((2.0*(s**2))**3)),2.0)*1.0)
                        #print "security", security_level
                        mq[4] = security_level
                        #abc.printPublicKey_ToolANF(pk,c[0],pedro,mq[4],mq[5])
                        #print "decrypt message", abc.decryptBlock(c[0],c[1],L1,L2,B,C, message)
                        abc.printPublicKey_ToolANF(pk, c[0], pedro, mq[5])
                        break;
                    except Exception, e:
                        print 'Error abc exception\n'    
            elif mq[0] == "sts":
                while True:
                    try:
		        L = mq[1]
                        r = mq[2]
                        pot = mq[3]
                        q = mq[4]
                        n = mq[5] 
                        sts = STS(L, r, pot, q, n)
                        sts.modePrint = 0  
                        k_lst = sts.keyGeneration()
                        pk_lst = k_lst[0]
                        S = k_lst[1]
                        T = k_lst[2]
                        sk_lst = k_lst[3]
                        """
                        #pretty_print("::::::::::::::::::::::::::::: Chave Privada :::::::::::::::::::::::")
                        #sts.printVarValue("T",T)
                        #sts.printVarValue("P'",sk_lst)
                        #sts.printVarValue("S",S)
                        #pretty_print("::::::::::::::::::::::::::::: Chave Publica :::::::::::::::::::::::")
                        #sts.printVarValue("P",pk_lst)
                        #"""
                        message = vector([sts.K.random_element() for _ in range(sts.n)])
                        ##sts.printVarValue("msg",message)
                        c = sts.encryptBlock(message, pk_lst)
                        pedro = ""
			print sts.n
                        for i in range(sts.n+1):
                            pedro = pedro + "0 "
                        sts.printPublicKey_ToolANF(pk_lst, c[0], pedro, mq[6])
                        break
                    except Exception, e:
                         print 'Error sts exception\n'    
                         print e
			 print traceback.print_stack()
            elif mq[0] == "hfe":
                while True:
                    try:
                        pot = mq[1]
                        r = mq[2]
                        q = mq[3]
                        hfe = HFE(r, pot, q)
                        x = hfe.K.random_element()
                        message = vector([x**(q**i) for i in range(hfe.pot)])
                        
                        pedro = ""
                        for i in range(hfe.pot+1):
                            pedro = pedro + "0 "
                         
                        pk = hfe.keyGeneration()
                        c = message*pk*message.column()
                        
                        hfe.printPublicKey_ToolANF(pk, c, pedro, mq[4])
                        break
                    except Exception, e:
                         print 'Error hfe exception\n' 
                         print e
                         return 0
            elif mq[0] == "uov":
                while True:
                    try:
                        q = mq[1]
                        pot = mq[2]
                        n = mq[3]
                        m = mq[4]
                        uov = UOV(q, pot, n, m)
                        x = uov.K.random_element()
                        sign = vector([x**(q**i) for i in range(n)])
                        [sk,pk] = uov.key_generation()
                        message = uov.evalua(pk, sign)
                        pedro = ""
                        for i in range(n+1):
                            pedro = pedro + "0 "
                        uov.printPublicKey_ToolANF(pk, message, pedro, mq[5])
                        break
                    except Exception, e:
                         print 'Error uov exception\n'
                         print e
                         return 0
                    
##defining output path for mq files
path_output = "/home/grados-sanchez/grive/LNCC/doutorado/projects/MPQCSAT/mq/sts_34/"
#L, r, pot, q, n
for i in range(101):
    MQWriter([["sts",48,6,1,2,48,i]])
    

